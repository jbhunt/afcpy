

def plot_emperical_density_function(data,
                                    test_value=None,
                                    ax=None,
                                    rug=True,
                                    cmap='viridis',
                                    alpha=1,
                                    lw=0.8):
    """
    plots the emperical density function for a given sample.
    
    keywords
    --------
    data : numpy array or list
        sample used to compute the EDF
    test_value : int or float, optional
        a value to be compared to the EDF; will be shown in the rugplot
    ax : matplotlib axes instance, optional (default is None)
        ax to be used for plotting
    rug : bool (default is True)
        if True will plot rugplot on the bottom of the EDF
    cmap : str (default is 'viridis')
        color map to be used for filling quartiles
    alpha : float (default is 1)
        specifies the alpha value for the quartile fills
    lw : int (default is 1)
        specifies the lw for the EDF and quartile boundaries
    
    """
    
    if ax is None:
        ax = figure().add_subplot(111)
        
    kdeplot(data,ax=ax,color='k')
    x,y = ax.get_lines()[0].get_data()
    
    cdf = cumtrapz(y,x,initial=0)
    nearest_025 = np.abs(cdf-0.25).argmin()
    nearest_05 = np.abs(cdf-0.5).argmin()
    nearest_075 = np.abs(cdf-0.75).argmin()
    
    x025, y025 = x[nearest_025], y[nearest_025]
    x05, y05 = x[nearest_05], y[nearest_05]
    x075, y075 = x[nearest_075], y[nearest_075]
    
    cmap = get_cmap(cmap,4)
    
    ax.fill_between(x[:nearest_025+1],0,y[:nearest_025+1],color=cmap(0), alpha=alpha)
    ax.fill_between(x[nearest_025:nearest_05+1],0,y[nearest_025:nearest_05+1],color=cmap(1), alpha=alpha)
    ax.fill_between(x[nearest_05:nearest_075+1],0,y[nearest_05:nearest_075+1],color=cmap(2), alpha=alpha)
    ax.fill_between(x[nearest_075:],0,y[nearest_075:],color=cmap(3), alpha=alpha)
    
    ax.vlines(x025,0,y025,'k')
    ax.vlines(x05,0,y05,'k')
    ax.vlines(x075,0,y075,'k')
    
    if rug is True:
        for datum in data:
            ax.vlines(datum,-0.25,0.25,'k',lw=lw)
        if test_value is not None:
            ax.vlines(test_value,-0.25,0.25,'r',lw=lw+1)
            
def histogram_time(dataset,
                   date_range=None,
                   cmap='plasma',
                   **kwargs):
    """
    """
        
    if date_range is None:
        session_dates = []
        for file in dataset:
            date = get_session_meta_data(file, field='date')
            session_dates.append(date)
        date_range = (np.min(session_dates), np.max(session_dates))
     
    session_subsets = []
    
    start, stop = date_range
    for date in range(start,stop+1):
        sessions = []
        for file in dataset:
            if str(date) in file:
                sessions.append(file)
        
        if len(sessions) == 0:
            session_subsets.append(np.nan)
            continue
        
        beta_coefs = []
        for session in sessions:
            beta_light = fit_choice_data_asymmetric(session,return_beta_opto=True)
            beta_coefs.append(beta_light)
            
        session_subsets.append(beta_coefs)
        
    n_days = len(session_subsets)
    cmap = get_cmap(cmap,n_days)
    colors = [cmap(i) for i in range(n_days)]
    
    ax = figure().add_subplot(111)
    ax.hist(session_subsets,color=colors,histtype='barstacked',**kwargs)
    
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    norm = mpl.colors.Normalize(vmin=start-start, vmax=stop-start)
    cb = mpl.colorbar.ColorbarBase(ax=cax,cmap=cmap,norm=norm,orientation='vertical')
    
def histogram_significance(dataset, cmap='gray', sig_level=0.05, n=100, **kwargs):
    """
    """
    data_sig = []
    data_not_sig = []
    for file in dataset:
        b_light, null_dist, p = session_bootstrap_hypothesis_test(file, n=n)
        if p <= sig_level:
            data_sig.append(b_light)
        else:
            data_not_sig.append(b_light)
    
    ax = figure().add_subplot(111)
    ax.hist([data_sig, data_not_sig],color=['k','w'], edgecolor='k', histtype='barstacked', **kwargs)
    
    return data_sig, data_not_sig