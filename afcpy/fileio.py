

def decode_odor_params(odor_params):
    """
    """
    
    # r_prob = odor_params[0][0]['R_prob'][0][0][0].reshape(-1,1)
    # l_prob = odor_params[0][0]['L_prob'][0][0][0].reshape(-1,1)
    # frac_of_trials = odor_params[0][0]['fraction_of_trials'][0][0][0].reshape(-1,1)
    # frac_odor_right = np.array([item[0][0] for item in odor_params[0][0]['odors'][0][0][0]]).reshape(-1,1)
    
    frac_odor_right = []
    # import pdb; pdb.set_trace()
    for odor in odor_params[0][0]['odors'][0][0][0]:
        odor = odor[0][0]
        if type(odor) is str: # sometimes the NaN values come out as empty strings.
            frac_odor_right.append(np.nan)
        elif odor > 0:
            frac_odor_right.append(odor)
        elif odor < 0:
            frac_odor_right.append(odor+100)
        else:
            frac_odor_right.append(np.nan)
         
    frac_odor_right = np.array(frac_odor_right).reshape(-1,1)
    frac_odor_left = np.full_like(frac_odor_right,100) - frac_odor_right
    ids = np.arange(0,frac_odor_left.size).reshape(-1,1) + 1
    params = np.concatenate([frac_odor_left,ids],1)
    params_df = pd.DataFrame(params,columns=['frac_odor_left','id'])
    params_df.dropna(inplace=True) # drop rows with NaN values.
    code_dict = dict(zip(params_df['frac_odor_left']/100,params_df['id']))
    
    return code_dict

def mat_to_df(mat, include_ee=True):
    """
    """
    
    # extract relevant fields.
    mat_open = loadmat(mat)
    try:
        taskbase = mat_open['taskbase']
    except:
        taskbase = mat_open['ans']
    stim = taskbase['stimID'][0][0]
    choice = taskbase['choice'][0][0]
    idx = ~np.isnan(choice)
    choice[idx] = choice[idx]
    
    # include trials in which the animal committed an out-early error, but still made a choice.
    if include_ee is True:
        choice_ee = taskbase['SideAFOE'][0][0]
        idx = ~np.isnan(choice_ee)
        choice[idx] = choice_ee[idx]
        
    opto = taskbase['fib_lit'][0][0]
    # trial_type = taskbase['HitHistory'][0][0]
    odor_params = taskbase['odor_params']
    
    # build the field containing the stimulus identity trial-by-trial.
    frac_odor_left = np.full(opto.size,0.)
    odor_code_dict = decode_odor_params(odor_params)
    for frac, id in odor_code_dict.items():
        mask = np.where(stim==int(id))[0]
        frac_odor_left[mask] = frac

    # assemble all fields into a data structure.
    
    # choice.
    idx1 = np.where(choice==1)[0] # left.
    idx2 = np.where(choice==2)[0] # right.
    idx = sorted(np.concatenate([idx1,idx2]))
    x_choice = choice[idx].reshape(-1,1) - 1 # 0: left; 1: right.
    x_choice = 1 - x_choice # 1: left; 0: right.
    
    # % odors left and right.
    frac_odor_left = frac_odor_left[idx].reshape(-1,1)
    frac_odor_right = 1-frac_odor_left
    
    # X odors left and right
    x_odor_left = (frac_odor_left-0.5)/0.5
    x_odor_right = (frac_odor_right-0.5)/0.5
    x_odor_left[x_odor_left<0] = 0
    x_odor_right[x_odor_right<0] = 0
    
    # light.
    x_opto = opto[idx].reshape(-1,1)
    
    # output.
    data_raw = np.concatenate([x_choice,x_odor_left,x_odor_right,frac_odor_left,frac_odor_right,x_choice,x_opto], axis=1)
    data = pd.DataFrame(data_raw[:,1:], columns=['x_odor_left','x_odor_right','%_odor_left','%_odor_right','x_choice','x_opto'])
    
    return datas