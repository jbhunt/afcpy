import os

def get_session_meta_data(filename, field='date'):
    """
    """
    
    # extract raw strings from file name.
    (prefix,
     protocol,
     experimenter,
     animal_id,
     side,
     stim_params,
     laser_intensity,
     date,
     file_type
     ) = filename.split('_')
     
    # process some of the raw strings.
    date = int(''.join([i for i in date if i.isalpha() is False]))
    
    # build dictionary.
    meta_dict = {'protocol':protocol,
                 'animal_id':animal_id,
                 'side':side,
                 'stim_params':stim_params,
                 'laser_intensity':laser_intensity,
                 'date':date}
    
    return meta_dict[field]

def generate_taskbase_files(base_dir):
    """
    searches for the unprocessed experiment files and uses a MATLAB
    function to generate the corresponding taskbase.mat file
    
    NOTE: This function depends on the MATLAB Engine package. This
    package comes with MATLAB 2014b and later, but the setup.py script
    needs to be manually run. For installation instructions refer to:
    
    https://www.mathworks.com/help/matlab/matlab_external/install-the-matlab-engine-for-python.html
    
    """
    
    all_disp_files = []
    
    for root, folders, files in os.walk(base_dir):
        for file in files:
            if file.endswith('.mat'):
                disp_file = os.path.join(root, file)
                if 'taskbase' not in disp_file:
                    all_disp_files.append(disp_file)
    
    eng = engine.start_matlab()
    for disp_file in all_disp_files:
        disp_file_basename, ext = os.path.splitext(os.path.basename(disp_file))
        disp_file_dir = os.path.dirname(disp_file)
        taskbase_basename = disp_file_basename+'_taskbase.mat'
        taskbase_full_path = os.path.join(disp_file_dir, taskbase_basename)
        if os.path.exists(taskbase_full_path) is False:
            eng.generate_taskbase(disp_file, taskbase_full_path)

def collect_taskbase_files(base_dir):
    """
    collects all of the taskbase files for either glu or arch animals 
    """
    
    all_taskbase_files = []
    
    for root, folders, files in os.walk(base_dir):
        for file in files:
            if file.endswith('taskbase.mat'):
                taskbase = os.path.join(root, file)
                all_taskbase_files.append(taskbase)
                    
    return all_taskbase_files

def collect_clean_dataset(base_dir, params_dict={'genotype':'glu',
                                                 'side':'ipsi',
                                                 'laser_intensity_range':(0,1),
                                                 'laser_frequency':[8,25],
                                                 'laser_pulse_width':[5,10],
                                                 'min_light_on_trials':50,
                                                 'from_date':None}):
    """
    collects all of the taskbase files contained within the specified root
    directory filtered by the parameters stored in the parameters dictionary.
    """
    
    all_taskbase_files = collect_taskbase_files(base_dir)
    file_dump = []

    # data_@M2L_Andrew_arch1_contra_500-490-10_3.0mW_190412a_taskbase.mat - example file naming convention.
    for file in all_taskbase_files:
        (prefix,
         protocol,
         experimenter,
         animal_id,
         side,
         stim_params,
         laser_intensity,
         date,
         file_type
         ) = file.split('_')
        
        stim_on_time, pulse_on_width, pulse_off_width = stim_params.split('-')
        stim_on_time, pulse_on_width, pulse_off_width = list(map(int, [stim_on_time, pulse_on_width, pulse_off_width]))
        frequency = (1000 / (1000 - stim_on_time)) * (stim_on_time / (pulse_on_width+pulse_off_width))
        laser_intensity = float(laser_intensity.rstrip('mW'))
        
        if ((params_dict['genotype'] in animal_id)
            & (side == params_dict['side'])
            & (laser_intensity >= params_dict['laser_intensity_range'][0])
            & (laser_intensity <= params_dict['laser_intensity_range'][1])
            & (pulse_on_width in params_dict['laser_pulse_width'])
            & (frequency in params_dict['laser_frequency'])
            ):
            pass
        
        else:
            continue
            
        if params_dict['from_date'] is not None:
            date = int(''.join([i for i in date if i.isalpha() is False]))
            if date >= params_dict['from_date']:
                pass
            else:
                continue
            
        df = mat_to_df(file)
        n_light_on_trials = df[df.x_opto==1].shape[0]
        if n_light_on_trials >= params_dict['min_light_on_trials']:
            file_dump.append(file)
            
    return file_dump       

def fetch_new_data():
    """
    """
    
    # hard-coded directories.
    base_dir_net = 'X:/Behavior/Data/Andrew/'
    base_dir_loc = 'C:/Users/jbhunt/Dropbox/Lab/Felsen/Data/'
    
    all_net_files = []
    
    for root, folders, files in os.walk(base_dir_net):
        for file in files:
            if file.endswith('.mat'):
                mat = os.path.join(root, file)
                if ('arch' in mat) | ('glu' in mat):
                    all_net_files.append(mat)
    
    animal_root_dirs = []
    
    for root, folders, files in os.walk(base_dir_loc):
        for folder in folders:
            if ('arch' in folder)|('glu' in folder):
                animal_root_dir = os.path.join(root,folder)
                animal_root_dirs.append(animal_root_dir)
            
    for animal_root_dir in animal_root_dirs:
        animal_id = os.path.basename(animal_root_dir)
        
        all_animal_net_files = []
        for file in all_net_files:
            if animal_id in file:
                all_animal_net_files.append(file)
        
        all_animal_local_files = []
        for root, folders, files in os.walk(animal_root_dir):
            for file in files:
                all_animal_local_files.append(file)
        
        for file in all_animal_net_files:
            basename = os.path.basename(file)
            if basename not in all_animal_local_files:
                print('transferring %s ...' % basename)
                full_file_path = os.path.abspath(file)
                dest_folder_path = os.path.join(base_dir_loc, animal_id)
                copy2(full_file_path, dest_folder_path)