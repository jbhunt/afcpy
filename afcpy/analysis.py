

def mat_to_frac_left(mat, opto_on=False):
    
    if opto_on is False:
        df = mat_to_df(mat)
        df = df[df.x_opto==0]
    else:
        df = mat_to_df(mat)
        df = df[df.x_opto==1]
        
    stim = np.array(df['%_odor_left'])
    choice = np.array(df['x_choice'])
    odors = np.unique(stim)
    
    idx_sets = []
    for odor in odors:
        idx = np.where(stim==odor)[0]
        idx_sets.append(idx)
        
    frac_left_all = [0,0,0,0,0,0,0]
    err_left_all = [0,0,0,0,0,0,0]
        
    for c, idx in enumerate(idx_sets):
        choices = choice[idx]
        frac = np.where(choices==1)[0].size / choices.size # left choice coded as 1.
        err = sem(choices)
        frac_left_all[c] = frac
        err_left_all[c] = err
        
    return np.array(frac_left_all), np.array(err_left_all)

def compute_simple_bias(mat):
    """
    """
    
    a, b = fit_choice_data_symmetric(mat, opto_on=False, return_params=True)
    bias_opto_off = (a/b + .5) * 100
    a, b = fit_choice_data_symmetric(mat, opto_on=True, return_params=True)
    bias_opto_on = (a/b + .5) * 100
    bias_opto_off_norm = bias_opto_off - bias_opto_on
    
    return bias_opto_off_norm

def get_choice_bias(mat, mode='simple'):
    """
    """
    
    if mode == 'simple':
        bias = compute_simple_bias(mat)
    elif mode == 'beta_coef':
        bias = fit_choice_data_asymmetric(mat, return_beta_opto=True)
        
    return bias

def sort_sessions_on_significance(dataset):
    """
    """
    
    # TODO: write function.
    
    return

def group_bootstrap_hypothesis_test(dataset, n=100, tail='right'):
    """
    """
    
    biases_beta_coef = []
    for file in dataset:
        bias = get_choice_bias(file, mode='beta_coef')
        biases_beta_coef.append(bias)
        
    test_mean = np.mean(biases_beta_coef)
    
    null_dist = []
    for i in range(n):
        bias_beta_coef_temp = []
        for file in dataset:
            df = mat_to_df(file)
            np.random.shuffle(df['x_opto'])
            X = df.loc[:,['x_odor_left','x_odor_right','x_opto']]
            y = df.loc[:,['x_choice']]
            (b_bias,
             b_odor_left,
             b_odor_right,
             b_opto
             ), pcov = curve_fit(complex_sigmoid_function, X, y['x_choice'])
            
            bias_beta_coef_temp.append(b_opto)
        
        shuffle_mean = np.mean(bias_beta_coef_temp)
        null_dist.append(shuffle_mean)
    
    if tail == 'right':
        p = np.array(null_dist)[null_dist>test_mean].size / n
    elif tail == 'left':
        p = np.array(null_dist)[null_dist<test_mean].size / n
    else:
        p = None
    
    return test_mean, null_dist, p

def session_bootstrap_hypothesis_test(session_id, n=100):
    """
    """
    
    df = mat_to_df(session_id)
    X = df.loc[:,['x_odor_left','x_odor_right','x_opto']]
    y = df.loc[:,['x_choice']]
                
    (b_bias,
     b_odor_left,
     b_odor_right,
     b_opto
     ), pcov = curve_fit(complex_sigmoid_function, X, y['x_choice'])
     
    b_opto = b_opto*-1 # reverse sign.
    
    beta_coefs_null_dist = []
    for i in range(n):
        np.random.shuffle(df['x_opto'])
        X_shuffle = df.loc[:,['x_odor_left','x_odor_right','x_opto']]
        
        try:
            (b_bias_shuffle,
             b_odor_left_shuffle,
             b_odor_right_shuffle,
             b_opto_shuffle
             ), pcov = curve_fit(complex_sigmoid_function, X_shuffle, y['x_choice'])
             
        except:
            continue
         
        b_opto_shuffle = b_opto_shuffle*-1 # reverse sign.
         
        beta_coefs_null_dist.append(b_opto_shuffle)
        
    p_right = np.array(beta_coefs_null_dist)[beta_coefs_null_dist>b_opto].size / n
    p_left = np.array(beta_coefs_null_dist)[beta_coefs_null_dist<b_opto].size / n
    
    if p_left < p_right:
        p = p_left
    elif p_right < p_left:
        p = p_right
    else:
        p = p_left
         
    return b_opto, beta_coefs_null_dist,  p