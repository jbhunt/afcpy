import numpy as np

def basic_sigmoid_function(x, a, b):
    """
    """
    
    y = 1/(1+np.e**(-a-(b*x)))
    return y

def complex_sigmoid_function(X,b_bias,b_odor_left,b_odor_right,b_opto):
    """
    """
    
    x_odor_left, x_odor_right, x_opto = X['x_odor_left'], X['x_odor_right'], X['x_opto']
    eta = b_bias + b_odor_left*x_odor_left + b_odor_right*x_odor_right + b_opto*x_opto
    y = 1 / (1+np.e**-eta)
    
    return y

def basic_sigmoid_function_corrected(x, a, b, l, g):
    """
    """
    
    y = 1/(1+np.e**(-a-(b*x)))
    correction = (1-l-g)
    y_corrected = l + y * correction
    
    return y_corrected

def fit_choice_data_symmetric(mat, opto_on=False, return_params=False, correct_for_lapse=False):
    
    """
    """
    
    df = mat_to_df(mat)
    if opto_on is True:
        df = df[df['x_opto']==1]
    else:
        df = df[df['x_opto']==0]
        
    if df.size <= 50:
        print('warning: number of trials with photostimulation is < 50.')
        return (0,0)
        
    elif df.size == 0:
        print('error: no trials with photostimulation detected.')
        return (0,0)
        
    stim = np.array(df['%_odor_left'])  
    choice = np.array(df['x_choice'])
    if correct_for_lapse is True:
        func = basic_sigmoid_function_corrected
    else:
        func = basic_sigmoid_function
    param_bounds = ([-np.inf,0],
                    [np.inf,100])
    popt, pcov = curve_fit(func, stim, choice, bounds=param_bounds)
    shape = func(np.arange(0,1,0.01), *popt)
    
    if return_params is False:
        return shape
    
    else: return popt

def fit_choice_data_asymmetric(mat, opto_on=False, return_beta_opto=False):
    
    """
    """
    
    df = mat_to_df(mat)
    X = df.loc[:,['x_odor_left','x_odor_right','x_opto']]
    y = df.loc[:,['x_choice']]
    
    popt, pcov = curve_fit(complex_sigmoid_function, X, y['x_choice'])
    
    if return_beta_opto is True:
        return popt[-1] * -1 # multiplied by -1 to reverse the sign (consistent with the sign of the simple bias metric).
    
    pad = np.zeros(50)
    x_odor_right = np.concatenate([np.arange(0,1.02,0.02)[::-1],pad])
    x_odor_left = np.concatenate([pad,np.arange(0,1.02,0.02)])
    x_opto_off = np.zeros_like(x_odor_right)
    x_opto_on = np.ones_like(x_odor_right)
    
    if opto_on is False:
        X = pd.DataFrame(np.array([x_odor_left,x_odor_right,x_opto_off]).T, columns=['x_odor_left','x_odor_right','x_opto'])
        shape = complex_sigmoid_function(X,*popt)
    else:
        X = pd.DataFrame(np.array([x_odor_left,x_odor_right,x_opto_on]).T, columns=['x_odor_left','x_odor_right','x_opto'])
        shape = complex_sigmoid_function(X, *popt)
        
    return shape